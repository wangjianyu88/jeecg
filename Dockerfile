FROM docker.xdja.com/ops/openresty:1.13.6.2
MAINTAINER  <hzp@xdja.com>
ADD jeecg-web.tar.gz /home/xdja/apps/web/
ADD nginx.conf  /usr/local/openresty/nginx/conf/http_vhost/
